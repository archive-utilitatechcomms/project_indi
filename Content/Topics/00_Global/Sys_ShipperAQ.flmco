﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>Shipper AQ</MicroContentPhrase>
        <MicroContentPhrase>Shipper</MicroContentPhrase>
        <MicroContentPhrase>AQ</MicroContentPhrase>
        <MicroContentPhrase>Nexus</MicroContentPhrase>
        <MicroContentPhrase>Annualised Quantity</MicroContentPhrase>
        <MicroContentPhrase>Annual Quantity</MicroContentPhrase>
        <MicroContentPhrase>Annual Agreed Quantity</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>System - Shipper AQ (Nexus)</xhtml:h1>
                    <xhtml:p>The <xhtml:b>Shipper AQ System</xhtml:b> is used primarily by the Gas Portfolio team at Utilita Energy to deal with situations where the Annualised (Annual) Quantity or Annual Agreed Quantity (AQ) is incorrect, or where there is a problem with a meter reading, such as where the usage of a site has changed significantly (for example a change of business type), in the event of gas theft, where incorrect historical data has led to a meter read being rejected, or where an incorrect meter read has led to an erroneous AQ.</xhtml:p>
                    <xhtml:p>The application allows users to perform a number of activities relating to the role of Shipper, including:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>Viewing the current AQ details and historical AQ information for a supply point.</xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li>Issuing a correction to a supply point's AQ.</xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li>Viewing meter technical details for a supply point.</xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li>Issuing changes to meter details (for example if a meter exchange has occurred or a new meter is installed).</xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li>Issuing replacement readings or resending rejected ones once the underlying issue has been resolved.</xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>You can find further information in our <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_Service_LL_AQ_Shipper_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="AQ / Shipper / Nexus DevOps Help Guide" xhtml:alt="AQ / Shipper / Nexus DevOps Help Guide" xhtml:target="_blank">AQ / Shipper / Nexus DevOps Help Guide</xhtml:a> and our <xhtml:a xhtml:href="http://ugl-jir-001-dev/TechPubs/Help_AQ_Shipper/Content/Topics/01_introduction/01a_welcome.htm" xhtml:title="AQ / Shipper / Nexus End-User Help Guide" xhtml:alt="AQ / Shipper / Nexus End-User Help Guide" xhtml:target="_blank">AQ / Shipper / Nexus End-User Help Guide</xhtml:a>.</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>