﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>DFM</MicroContentPhrase>
        <MicroContentPhrase>Data Flows</MicroContentPhrase>
        <MicroContentPhrase>Data Flow</MicroContentPhrase>
        <MicroContentPhrase>Dataflow</MicroContentPhrase>
        <MicroContentPhrase>Dataflow&#160;Manager</MicroContentPhrase>
        <MicroContentPhrase>Data Flow&#160;Manager</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>System - Data Flows &amp; Dataflow&#160;Manager (DFM)</xhtml:h1>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Data Flows</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Data flows are used by the electricity and gas industries to communicate information about customer meters between interested industry parties. They can cover a wide variety of information from meter readings to change of supplier details, updating energisation status to debt transfer.</xhtml:p>
                            <xhtml:p>For more information about our data flows and Dataflow Manager, refer to the <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_04_TP_Data_Flows/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="Dataflow Manager DevOps Help Guide" xhtml:alt="Dataflow Manager DevOps Help Guide" xhtml:target="_blank">Dataflow Manager DevOps Help Guide</xhtml:a>. </xhtml:p>
                            <xhtml:p>The format and structure of the content of all data flows is defined in the <xhtml:a xhtml:href="https://dtc.mrasco.com/Default.aspx" xhtml:target="_blank" xhtml:title="MRA Data Transfer Catalogue" xhtml:alt="MRA Data Transfer Catalogue">MRA Data Transfer Catalogue</xhtml:a>. Different industry participants (supplier, distributor, meter operator, etc.) are responsible for sending and receiving a variety of different data flows specific to their roles in the industry.</xhtml:p>
                            <xhtml:p>In Utilita's case, we send and receive data flows as a supplier, a <xhtml:a xhtml:href="Proc_MOP.htm" xhtml:target="_blank" xhtml:title="MOP (Meter Operator)" xhtml:alt="MOP (Meter Operator)">MOP (Meter Operator)</xhtml:a> and a <xhtml:a xhtml:href="Proc_MAP.htm" xhtml:target="_blank" xhtml:title="MAP (Meter Asset Provider)" xhtml:alt="MAP (Meter Asset Provider)">MAP (Meter Asset Provider)</xhtml:a>.</xhtml:p>
                            <xhtml:p>The diagram below shows which data flows are sent and received by Utilita as each of these industry roles. Click on a data flow number to find out further information about the purpose, structure and content of that flow.</xhtml:p>
                            <xhtml:p>
                                <xhtml:img xhtml:src="00_IMG_utilita_data_flows.png" xhtml:class="Screen_Thumbnail" />
                            </xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Dataflow Manager</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The Dataflow Manager application is used to import and export data files into and out of Utilita's internal systems. It's designed to operate as a "lights out" service, polling various data sources for triggers and completing scheduled jobs, rather than requiring manual operation and intervention.</xhtml:p>
                            <xhtml:p>For more information about our data flows and Dataflow Manager, refer to the <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_04_TP_Data_Flows/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="Dataflow Manager DevOps Help Guide" xhtml:alt="Dataflow Manager DevOps Help Guide" xhtml:target="_blank">Dataflow Manager DevOps Help Guide</xhtml:a>. </xhtml:p>
                            <xhtml:p>In particular, it is responsible for the transfer of data relating to customers either gained or lost through the <xhtml:a xhtml:href="Proc_COS.htm" xhtml:target="_blank" xhtml:title="Change of Supplier (COS)" xhtml:alt="Change of Supplier (COS)">Change of Supplier (CoS)</xhtml:a> process.</xhtml:p>
                            <xhtml:p>For customers being lost to other suppliers, it reads the appropriate <xhtml:code>ELEC_OUTGOING</xhtml:code> and <xhtml:code>GAS_OUTGOING</xhtml:code> tables ​where individual records are held for each customer and posts them via FTP to the appropriate industry operator, Electralink's Date Transfer Network (DTN) or <xhtml:a xhtml:href="Comp_Xoserve.htm" xhtml:target="_blank" xhtml:title="Xoserve" xhtml:alt="Xoserve">Xoserve</xhtml:a> for electricity and gas respectively. From here, the information is forwarded to the gaining supplier.</xhtml:p>
                            <xhtml:p>For customers being gained from other suppliers, the Dataflow Manager receives daily batch files from the DTN, one for electricity customers (<xhtml:code>ELEC_INCOMING</xhtml:code>) and one for gas customers (<xhtml:code>GAS_INCOMING</xhtml:code>). It then calls the relevant procedures to add these customer records to the appropriate database tables so that they are imported into Utilita's systems and <xhtml:a xhtml:href="Sys_CRM.htm" xhtml:target="_blank" xhtml:title="CRM" xhtml:alt="CRM">CRM</xhtml:a> is made aware of the customers who are scheduled to go live with Utilita in the coming days.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>